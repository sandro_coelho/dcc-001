#include <iostream>
#include "io/FileManager.hpp"
using namespace std;

int main(char* argv[])
{

    FileManager *fm = new FileManager();

    string str = fm->readFile(argv[0]);
    vector<int> vt = fm->toVector(str);

    int temp;

    for(size_t  i=vt.size() - 1; i > 0; i --)
    {
        for(size_t  j=0; j< i; j++)
        {
           if (vt[j+1] < vt[j]) {
              temp = vt[j];
              vt[j] = vt[j+1];
              vt[j+1] = temp;
           }
        }
    }

    return 0;
}
