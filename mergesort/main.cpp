#include <iostream>
#include <vector>
#include <algorithm>
#include "io/FileManager.hpp"

using namespace std;

template<class Iter>
void merge_sort(Iter first, Iter last)
{
    if (last - first > 1)
    {
        Iter middle = first + (last - first) / 2;
        merge_sort(first, middle);
        merge_sort(middle, last);
        std::inplace_merge(first, middle, last);
    }
}
int main(char* argv[])
{

    FileManager *fm = new FileManager();

    string str = fm->readFile(argv[0]);
    vector<int> v = fm->toVector(str);
    merge_sort(v.begin(), v.end());

    return 0;
}
