#ifndef FILEMANAGER_HPP_INCLUDED
#define FILEMANAGER_HPP_INCLUDED

#include <iostream>
#include <string>
#include <sstream>
#include <iterator>
#include <fstream>
#include <vector>
#include <cstdlib>

using namespace std;

class FileManager
{

private:

protected:

public:

    FileManager(){}


    /**
    * Efetua a leitura de um arquivo
    * @arquivo : nome do arquivo
    */
    string readFile(char arquivo[])
    {

        ifstream file(arquivo);
        string str;
        string file_contents;

        getline(file, str);

        return str;

    };

    vector<int> toVector(string str)
    {

        vector<int>  result;
        stringstream ss(str);
        int n;
        char ch;

        while(ss >> n)
        {
            if(ss >> ch)
            {
                result.push_back(n);

            }
            else
            {
                result.push_back(n);
            }
        }

        return result;
    }

};

#endif // FILEMANAGER_HPP_INCLUDED
