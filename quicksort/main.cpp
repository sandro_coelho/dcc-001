#include <iostream>
#include <algorithm>
#include <vector>
#include "io/FileManager.hpp"

using namespace std;

int main(char* argv[])
{
    FileManager *fm = new FileManager();

    string str = fm->readFile(argv[0]);
    vector<int> v = fm->toVector(str);

    sort (v.begin(), v.end());

    return 0;
}
